import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      children: [
        {
          path: '',
          name:'index',
          component: () => import('./views/Index.vue'),
        },
        {
          path: '/cart',
          name: 'cart',
          component: () => import('./views/Cart.vue'),
        },
        
        {
          path: '/search',
          name: 'search',
          component: () => import('./views/Search.vue'),
        },
        
        {
          path: '/my',
          name: 'my',
          component: () => import('./views/My.vue'),
        },
      ]
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/About.vue')
    },{
      path: '/result',
      name: 'result',
      component: () => import('./views/Result.vue'),
    },
  ]
})
