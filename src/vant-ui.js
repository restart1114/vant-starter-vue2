import Vue from 'vue';
import { Collapse, CollapseItem, Pagination, Step, Steps, Search, DropdownMenu, DropdownItem, Divider, List, Lazyload,Row, Col,Sidebar, SidebarItem,Grid, GridItem,Tab, Tabs,Tabbar, TabbarItem,Panel,Button,Uploader,NavBar,Toast,Dialog,NoticeBar,Tag,CountDown,Popup} from 'vant';
import { Loading } from 'vant';
import {Card, Swipe, SwipeItem, Form, Cell, CellGroup,Field,RadioGroup, Radio,SwitchCell,Checkbox, CheckboxGroup,DatetimePicker,Stepper,Picker} from 'vant';
import {Icon,Image,Circle } from 'vant';
Vue.use(Card);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Pagination);
Vue.use(Step);
Vue.use(Steps);
Vue.use(Search);
Vue.use(Search);
Vue.use(Row).use(Col);
Vue.use(DropdownMenu).use(DropdownItem);
Vue.use(Sidebar).use(SidebarItem);
Vue.use(Grid).use(GridItem);
Vue.use(Tab).use(Tabs);
Vue.use(Tabbar).use(TabbarItem);
Vue.use(Panel);
Vue.use(Button);
Vue.use(Uploader);
Vue.use(Toast);
Vue.use(Dialog);
Vue.use(NavBar);
Vue.use(NoticeBar);
Vue.use(Tag);
Vue.use(CountDown);
Vue.use(Popup);
Vue.use(Divider);
Vue.use(List);

Vue.use(Loading);
Vue.use(Form);
Vue.use(Cell).use(CellGroup);
Vue.use(Field);
Vue.use(RadioGroup);
Vue.use(Radio);
Vue.use(Checkbox).use(CheckboxGroup);
Vue.use(SwitchCell);
Vue.use(DatetimePicker);
Vue.use(Stepper);
Vue.use(Picker);

Vue.use(Icon);
Vue.use(Image);
Vue.use(Circle);