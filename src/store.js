import Vue from 'vue'
import Vuex from 'vuex'
import { api } from './api'

const remote = process.env.VUE_APP_REMOTE == '1'

const HOST = process.env.VUE_APP_IP

Vue.use(Vuex)
if (!remote) {
  localStorage.ddKey = 'nmlt'
}
export default new Vuex.Store({
  state: {
    host: HOST,
    remote:remote,
    indexNum:0,
    dduser: remote ? null : { userid: '09441216131253154', upDepart:319411090, name: '高健', mobile: '18698401720', avatar: 'https://static.dingtalk.com/media/lADPBbCc1a_esH_NAkDNAkA_576_576.jpg', points: '16', leader: true, point:0, god:1, ukey:'nmlt', },
    // dduser: null,
  },
  mutations: {
    updateIndexNum(state, payload) {
      state.indexNum = payload
    },
    updateDduser(state, payload) {
      state.dduser = payload
    },
    updateJsconfig(state, payload) {
      state.jsconfig = payload
    },
  },
  actions: {
    /* dingding相关 */
    fetchCorpid(context, payload) {
      console.log('corpid', `${HOST}/v1/dd/corpid`)
      return api(`${HOST}/v1/dd/corpid`)
    },
    fetchUserinfo(context, payload) {
      return api(`${HOST}/v1/dd/userinfo?code=${payload.code}`)
    },   
    fetchJsconfig({ state }, payload) {
      return api(`${HOST}/v1/dd/jsconfig`, { params: payload })
    },

    fetchMyMeetingCount(context, payload) {
      return api(`${HOST}/v1/meeting/count`, { params: payload })
    },
    fetchMeetingList(context, payload) {
      return api(`${HOST}/v1/meeting/index`, { params: payload })
    },
    fetchMeetingOne(context, payload) {
      return api(`${HOST}/v1/meeting/${payload.meetingCode}`, { params: payload })
    },
    fetchMeetingPartiOne(context, payload) {
      return api(`${HOST}/v1/meeting-parti/${payload.meetingCode}`, { params: payload })
    },
    saveMeeting(context, payload) {
      return api(`${HOST}/v1/meeting/save`, { method:'post', data: payload })
    },
    finishMeeting(context, payload) {
      return api(`${HOST}/v1/meeting/finish`, { method:'put', data: payload })
    },
    getConfirmResult(context, payload) {
      return api(`${HOST}/v1/meeting-parti/confirm`, { method:'post', data: payload })
    },
    getAvatarSynchroed(context, payload) {
      return api(`${HOST}/v1/meeting-parti/synchroed`, { method:'post', data: payload })
    }
  }
})
