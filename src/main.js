import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@vant/touch-emulator'
import vConsole from 'vconsole'
import { Lazyload } from 'vant';
import './vant-ui'
// new vConsole()
Vue.use(Lazyload);
Vue.config.productionTip = false
let query = parseQueryString();
if (query.key) {
  localStorage.ddKey = query.key
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
